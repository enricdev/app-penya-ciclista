
var storage = window.localStorage;
var pass = storage.getItem('pass');
var usuari = storage.getItem('usuari');

var cos = document.getElementById('cos');
var btnloc = document.getElementById('btn-loc');
var btndesa = document.getElementById('btn-desa');
var coord = document.getElementById('coord');
var btnlogin = document.getElementById('btn-login');
var pagetwo = document.getElementById('pagetwo');
var pagelogin = document.getElementById('loginpage');

var host = "http://penyaciclistapobla.com.mialias.net";

/* Funcions */
function getById(id){
  var element = document.getElementById(id);
  return element;
}

function datahui(){
  var today = new Date();

  var dd = today.getDate();
  var mm = today.getMonth()+1;//January is 0, so always add + 1

  var yyyy = today.getFullYear();
  if(dd<10){dd='0'+dd}
  if(mm<10){mm='0'+mm}
  today = dd+'/'+mm+'/'+yyyy;

  return today;
}

/* JSON Rutes */
let urlrutes = host+'/wp-admin/admin-ajax.php?action=rutes';

var rutadehui;
fetch(urlrutes)
.then(res => res.json())
.then((out) => {
  var len = out.length;
    var ruta = out;
    if(ruta['dataruta'] == datahui()){
      rutadehui = ruta;
  }
}).catch(err => console.error(err));


/* Inici de sessió */
if(pass != ''){
  getById('password').value = pass;
}

if(usuari != ''){
  getById('username').value = usuari;
}

btnlogin.onclick = function login(){
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    if(username == "")
    {
        navigator.notification.alert("Introduïx el teu nom d'usuari", null, "Falta el nom d'usuari", "OK");
        return;
    }

    if(password == "")
    {
        navigator.notification.alert("Introduïx la teua contrasenya", null, "Falta la contrasenya", "OK");
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("GET", host+"/wp-admin/admin-ajax.php?action=login&username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password));
    xhr.onload = function(){
        if(xhr.responseText == "FALSE")
        {
            navigator.notification.alert("El nom d'usuari o la contrasenya són incorrectes", null, "Dades incorrectes", "Intenta-ho de nou");
        }
        else
        {

            storage.setItem('usuari', username);
            storage.setItem('pass', password);

              var jsonsoci = JSON.parse(xhr.responseText);
              storage.setItem('idsoci', jsonsoci['idsoci']);

              nextpage();

              getById('btn-back').classList.remove('op0');

              getById('benvinguda').innerHTML = "Hola "+jsonsoci['nom']+".";

              if(rutadehui){
                getById('benvinguda').innerHTML += "<br> La ruta de hui és a <strong>"+rutadehui['title']+"</strong>. Si estàs al lloc de l'esmorzar pots fer validar la teua assistència a la ruta.";
                getById('loc-cont').classList.remove('hidden');
                getById('loc-cont').classList.add('visible');
              } else {
                getById('benvinguda').innerHTML += "<br> Hui no hi ha cap ruta programada";
              }
        }
    }
    xhr.send();
}

var idsoci = storage.getItem('idsoci');

function truncate(num, places) {
  return Math.trunc(num * Math.pow(10, places)) / Math.pow(10, places);
}

function valida_coord(cd1, cd2){

  var trcd1 = truncate(cd1,2);
  var trcd2 = truncate(cd2,2);

  if(trcd1 + 0.01 == trcd2 || trcd1 - 0.01 == trcd2 || trcd1 == trcd2){
    return true;
  } else {
    return false;
  }

}

var onSuccess = function(position) {
  var lat = parseFloat(position.coords.latitude);
  var lng = parseFloat(position.coords.longitude);

  var latesm = parseFloat(rutadehui['esmorzar']['lat']);
  var lngesm = parseFloat(rutadehui['esmorzar']['lng']);

  if(valida_coord(lat, latesm) && valida_coord(lng, lngesm)){
		coord.innerHTML = "Validació correcta. Polsa el botó «He assistit» per confirmar la teua assistència";
    btndesa.removeAttribute('disabled');
	} else {
    coord.innerHTML = "Validació incorrecta :( No estàs al lloc correcte o no hem pogut localitzar-te.<br>";
    // coord.innerHTML += truncate(lat,2)+"<br>"+truncate(lng,2);
  }

};

// onError Callback receives a PositionError object
function onError(error) {
    navigator.notification.alert("No hem pogut localitzar-te. Revisa si tens activat el GPS o la connexió a Internet.");
    console.log('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}


btnloc.onclick = function(){
  navigator.geolocation.getCurrentPosition(onSuccess, onError, { timeout: 10000 });
}

btndesa.onclick = function() {

    var xmlhttp = new XMLHttpRequest();
    var urlval = host+"/wp-admin/admin-ajax.php?action=desa&idsoci="+idsoci;

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
           if (xmlhttp.status == 200) {
             alert(xmlhttp.responseText);
             btndesa.setAttribute('disabled', 'disabled');
           }
           else if (xmlhttp.status == 400) {
              alert('There was an error 400');
           }
           else {
               alert('something else other than 200 was returned '+ xmlhttp.status);
           }
        }
    };

    xmlhttp.open("GET", urlval, true);
    xmlhttp.send();

}

getById('btn-prox').onclick = function(){

  var xmlhttp = new XMLHttpRequest();
  var urlval = host+"/wp-admin/admin-ajax.php?action=proximesrutes";

  xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
         if (xmlhttp.status == 200) {
           var proxrutes = JSON.parse(xmlhttp.responseText);
           console.log(proxrutes);

           if(proxrutes){

             nextpage();
             console.log(proxrutes);

             getById('page-prox').innerHTML = "Estes són les pròximes rutes previstes";
             for(var i = 0; i < proxrutes.length; i++){
               var nompruta = proxrutes[i]['title'];
               var datapruta = proxrutes[i]['dataruta'];
               var horapruta = proxrutes[i]['horaruta'];
               var trajpruta = proxrutes[i]['trajecte'];
               getById('page-prox').innerHTML += '<div class="prox-ruta">'+
               '<h2 class="nom-ruta">'+nompruta+'</h2><div class="traj">'+trajpruta+'</div>'+
               '<div class="data-hora"><p>'+datapruta+'</p>'+
               '<p>'+horapruta+'</p></div>'+
               '</div>';
             }
           } else {
             alert("No s'han trobat pròximes rutes");
           }
         }
         else if (xmlhttp.status == 400) {
            alert('There was an error 400');
         }
         else {
             alert('something else other than 200 was returned '+ xmlhttp.status);
         }
      }
  };

  xmlhttp.open("GET", urlval, true);
  xmlhttp.send();

}

getById('btn-back').onclick = function(){
  var activa = document.getElementsByClassName('center');
  var left = document.getElementsByClassName('left');
  var idact = activa[0].id;
  var previd = getById(idact).previousElementSibling.id;

  document.body.className = '';
  document.body.classList.add(previd);

  if(previd == 'loginpage'){
    this.classList.add('op0');
  }

  getById(idact).classList.remove('center');
  getById(idact).classList.add('right');
  getById(idact).previousElementSibling.classList.remove('left');
  getById(idact).previousElementSibling.classList.add('center');
}

function nextpage(){

  var activa = document.getElementsByClassName('center');
  var next = document.getElementsByClassName('right');
  var idact = activa[0].id;
  var nextid = getById(idact).nextElementSibling.id;

  document.body.className = '';
  document.body.classList.add(nextid);

  getById(idact).classList.remove('center');
  getById(idact).classList.add('left');
  getById(idact).nextElementSibling.classList.remove('right');
  getById(idact).nextElementSibling.classList.add('center');

}
